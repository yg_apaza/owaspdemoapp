package org.epis.owasp.sample.controller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import org.epis.owasp.SecuredMessage;

public class ClientSocket
{
    protected Socket cs;
    protected DataOutputStream salidaServidor;
    
    public ClientSocket(String host, int port) throws IOException
    {
        cs = new Socket(host, port);
    }

    public void sendMessage(SecuredMessage message)
    {
        try
        {
            OutputStream os = cs.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(message);
            oos.close();
            os.close();
            cs.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
