package org.epis.owasp.sample.controller;

import javax.swing.JTextField;

public interface IAddContact
{
    public void aceptar(JTextField txtAlias);
    public void salir(boolean closing);
}
