package org.epis.owasp.sample.controller;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

public interface IAuthentication
{
    public void cargarKeystore(JTextField txtKeystore);
    public void test(JTextField txtKeystore, JPasswordField txtKeystorePassword, JTextField txtAlias, JPasswordField txtAliasPassword);
    public void ok(JTextField txtKeystore, JPasswordField txtKeystorePassword, JTextField txtAlias, JPasswordField txtAliasPassword);
    public void salir(boolean closing);
}
