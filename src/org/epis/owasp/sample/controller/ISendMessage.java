package org.epis.owasp.sample.controller;

import javax.swing.JTextArea;
import javax.swing.JTextField;

public interface ISendMessage
{
    public void aceptar(JTextField txtIP, JTextArea txtMensaje, JTextField txtPuerto);
    public void salir(boolean closing);
}
