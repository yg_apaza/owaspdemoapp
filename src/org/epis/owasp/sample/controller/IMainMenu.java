package org.epis.owasp.sample.controller;

import javax.swing.JList;

public interface IMainMenu
{
    public void agregarContacto();
    public void showContacts(JList<String> lstContacts);
    public void showMessages(JList<String> lstMessages);
    public void enviarMensaje(JList<String> lstContacts);
    public void verMensaje(JList<String> lstMessages);
    public void salir(boolean closing);
}
