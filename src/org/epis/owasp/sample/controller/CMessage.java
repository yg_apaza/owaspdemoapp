package org.epis.owasp.sample.controller;

import java.awt.Color;
import java.security.PublicKey;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.sample.view.UIMessage;
import org.epis.owasp.OWASPAlgorithm;
import org.epis.owasp.OWASPException;
import org.epis.owasp.SecuredMessage;

public class CMessage implements IMessage
{
    private UIMessage ventana;
    private int message;
    
    public CMessage(int message)
    {
        this.message = message;
        this.ventana = new UIMessage(this);
    }
    
    @Override
    public void salir(boolean closing)
    {
        if(!closing)
            this.ventana.dispose();
        new CMainMenu();
    }

    @Override
    public void showMessageData(JTextField txtFirmaDigital, JTextField txtHashing, JTextArea txtMensaje)
    {
        try
        {
            SecuredMessage m = OWASPDemoApp.messages.get(message);
            boolean digitalSignature = false;
            
            
            Iterator it = OWASPDemoApp.contacts.keySet().iterator();
            while(it.hasNext())
            {
                String contact = (String)it.next();
                PublicKey verify = OWASPDemoApp.contacts.get(contact);
                if(OWASPAlgorithm.validateSignature(m, verify, "SHA256withRSA"))
                {
                    digitalSignature = true;
                    txtFirmaDigital.setText("|VERIFICADO| Firmado digitalmente por: " + contact);
                    txtFirmaDigital.setBackground(new Color(152, 242, 158));
                }
            }
            
            if(!digitalSignature)
            {
                txtFirmaDigital.setText("|INVÁLIDO| La firma digital no pudo ser verificada con ningún contacto");
                txtFirmaDigital.setBackground(new Color(255, 181, 181));
            }
            
            String mensaje = OWASPAlgorithm.unpackMessage(m, OWASPDemoApp.myPrivateKey);
            txtMensaje.setText(mensaje);
            
            boolean hashing = OWASPAlgorithm.validateHashing(m, mensaje, "SHA-256");
            if(hashing)
            {
                txtHashing.setText("|VERIFICADO| El mensaje no ha sido alterado");
                txtHashing.setBackground(new Color(152, 242, 158));
            }
            else
            {
                txtHashing.setText("|INVÁLIDO|El mensaje ha sido alterado");
                txtHashing.setBackground(new Color(255, 181, 181));
            }
        }
        catch (OWASPException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Error: " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
}