package org.epis.owasp.sample.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.SecuredMessage;

public class ServerSocket
{
    protected java.net.ServerSocket ss;
    protected Socket cs;
    
    public ServerSocket(int port) throws IOException
    {
        ss = new java.net.ServerSocket(port);
        cs = new Socket();
    }

    public void startServer()
    {
        try
        {
            System.out.println("Servidor socket iniciado");
            while (true)
            {
                cs = ss.accept();
                System.out.println("Nuevo cliente !");
                InputStream is = cs.getInputStream();
                ObjectInputStream ois = new ObjectInputStream(is);
                SecuredMessage secM = (SecuredMessage)ois.readObject();
                if(secM != null)
                    OWASPDemoApp.messages.add(secM);

                is.close();
                cs.close();
            }
        }
        catch (IOException | ClassNotFoundException ex)
        {
            System.out.println("Servidor cerrado. " + ex.getMessage());
        }
    }
    
    public void close()
    {
        try
        {
            ss.close();
        }
        catch (IOException ex)
        {
            System.out.println("No se pudo cerrar el servidor.");
        }
    }
}
