package org.epis.owasp.sample.controller;

import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.sample.view.UIMainMenu;

public class CMainMenu implements IMainMenu
{
    private UIMainMenu ventana;
    
    public CMainMenu()
    {
        this.ventana = new UIMainMenu(this);
    }

    @Override
    public void showContacts(JList<String> lstContacts)
    {
        DefaultListModel model = new DefaultListModel();
        Iterator it = OWASPDemoApp.contacts.keySet().iterator();
        while(it.hasNext())
            model.addElement((String) it.next());
        lstContacts.setModel(model);
    }

    @Override
    public void agregarContacto()
    {
        this.ventana.dispose();
        new CAddContact();
    }

    @Override
    public void enviarMensaje(JList<String> lstContacts)
    {
        if(lstContacts.getSelectedValue() != null)
        {
            new CSendMessage(lstContacts.getSelectedValue());
            this.ventana.dispose();
        }
        else
            JOptionPane.showMessageDialog(ventana, "Selecciona un contacto", "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void showMessages(JList<String> lstMessages)
    {
        DefaultListModel model = new DefaultListModel();
        for(int i = 0; i < OWASPDemoApp.messages.size(); i++)
            model.addElement(OWASPDemoApp.messages.get(i).getHeader().getHashMessage());
        lstMessages.setModel(model);
    }

    @Override
    public void verMensaje(JList<String> lstMessages)
    {
        if(lstMessages.getSelectedIndex() != -1)
        {
            new CMessage(lstMessages.getSelectedIndex());
            this.ventana.dispose();
        }
        else
            JOptionPane.showMessageDialog(ventana, "Selecciona un mensaje", "ERROR", JOptionPane.ERROR_MESSAGE);
    }
    
    @Override
    public void salir(boolean closing)
    {
        if(!closing)
            this.ventana.dispose();
        new CAuthentication();
    }
}
