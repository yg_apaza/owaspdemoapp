package org.epis.owasp.sample.controller;

import javax.swing.JTextArea;
import javax.swing.JTextField;

public interface IMessage
{
    public void salir(boolean closing);
    public void showMessageData(JTextField txtFirmaDigital, JTextField txtHashing, JTextArea txtMensaje);
}
