package org.epis.owasp.sample.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.sample.view.UIAuthentication;

public class CAuthentication implements IAuthentication
{
    private UIAuthentication ventana;
    private File fileKeystore;
    
    public CAuthentication()
    {
        OWASPDemoApp.keyStore = null;
        OWASPDemoApp.myPublicKey = null;
        OWASPDemoApp.myPrivateKey = null;
        OWASPDemoApp.messages.clear();
        OWASPDemoApp.contacts = new HashMap<String, PublicKey>();
        this.ventana = new UIAuthentication(this);
    }

    @Override
    public void cargarKeystore(JTextField txtKeystore)
    {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos keystore", "ks");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(ventana);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            this.fileKeystore = chooser.getSelectedFile();
            txtKeystore.setText(this.fileKeystore.getAbsolutePath());
        }
    }
    
    @Override
    public void salir(boolean closing)
    {
        if(!closing)
            this.ventana.dispose();
        OWASPDemoApp.server.close();
    }

    @Override
    public void test(JTextField txtKeystore, JPasswordField txtKeystorePassword, JTextField txtAlias, JPasswordField txtAliasPassword)
    {
        try
        {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] password = txtKeystorePassword.getPassword();
            FileInputStream fis = new FileInputStream(txtKeystore.getText());
            keystore.load(fis, password);
            fis.close();
            
            char[] keypassword = txtAliasPassword.getPassword();
            Key myKey = keystore.getKey(txtAlias.getText(), keypassword);
            if(myKey == null)
                throw new NullPointerException();
            
            X509Certificate sendercert = (X509Certificate) keystore.getCertificate(txtAlias.getText());
            if(sendercert == null)
                throw new NullPointerException();
            
            JOptionPane.showMessageDialog(ventana, "Datos válidos", "CORRECTO", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Keystore no válido: " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Archivo no encontrado", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (IOException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Contraseña del keystore inválida", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (UnrecoverableKeyException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Contraseña del alias no válido", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (NullPointerException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Alias no existe: " + txtAlias.getText(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void ok(JTextField txtKeystore, JPasswordField txtKeystorePassword, JTextField txtAlias, JPasswordField txtAliasPassword)
    {
        try
        {
            OWASPDemoApp.keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] password = txtKeystorePassword.getPassword();
            FileInputStream fis = new FileInputStream(txtKeystore.getText());
            OWASPDemoApp.keyStore.load(fis, password);
            fis.close();
            
            char[] keypassword = txtAliasPassword.getPassword();
            Key key = OWASPDemoApp.keyStore.getKey(txtAlias.getText(), keypassword);
            if(key == null)
                throw new NullPointerException();
            OWASPDemoApp.myPrivateKey = (PrivateKey) key;
            
            X509Certificate cert = (X509Certificate) OWASPDemoApp.keyStore.getCertificate(txtAlias.getText());
            if(cert == null)
                throw new NullPointerException();
            OWASPDemoApp.myPublicKey = cert.getPublicKey();
            new CMainMenu();
            this.ventana.dispose();
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Keystore no válido: " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Archivo no encontrado", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (IOException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Contraseña del keystore inválida", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (UnrecoverableKeyException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Contraseña del alias no válido", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (NullPointerException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Alias no existe: " + txtAlias.getText(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
}
