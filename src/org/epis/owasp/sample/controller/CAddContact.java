package org.epis.owasp.sample.controller;

import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.sample.view.UIAddContact;

public class CAddContact implements IAddContact
{
    private UIAddContact ventana;
    
    public CAddContact()
    {
        this.ventana = new UIAddContact(this);
    }
    
    @Override
    public void salir(boolean closing)
    {
        if(!closing)
            this.ventana.dispose();
        new CMainMenu();
    }
    
    @Override
    public void aceptar(JTextField txtAlias)
    {
        try
        {
            X509Certificate cert = (X509Certificate) OWASPDemoApp.keyStore.getCertificate(txtAlias.getText());
            if(cert == null)
                throw new NullPointerException();
            OWASPDemoApp.contacts.put(txtAlias.getText(), cert.getPublicKey());
            this.ventana.dispose();
            new CMainMenu();
        }
        catch (KeyStoreException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Keystore no válido: " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (NullPointerException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Alias no existe: " + txtAlias.getText(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
}
