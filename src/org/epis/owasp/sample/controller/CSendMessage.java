package org.epis.owasp.sample.controller;

import java.io.IOException;
import java.security.PublicKey;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.epis.owasp.sample.OWASPDemoApp;
import org.epis.owasp.sample.view.UISendMessage;
import org.epis.owasp.OWASPAlgorithm;
import org.epis.owasp.OWASPException;
import org.epis.owasp.SecuredMessage;

public class CSendMessage implements ISendMessage
{
    private UISendMessage ventana;
    private String contact;
    
    public CSendMessage(String contact)
    {
        this.contact = contact;
        this.ventana = new UISendMessage(this);
    }

    @Override
    public void aceptar(JTextField txtIP, JTextArea txtMensaje, JTextField txtPuerto)
    {
        try
        {
            int port = Integer.parseInt(txtPuerto.getText());
            ClientSocket client = new ClientSocket(txtIP.getText(), port);
            PublicKey pubKeyReceiver = OWASPDemoApp.contacts.get(this.contact);
            SecuredMessage message = OWASPAlgorithm.packMessage(txtMensaje.getText(), OWASPDemoApp.myPrivateKey, pubKeyReceiver, "SHA-256", "SHA256withRSA");
            client.sendMessage(message);
            new CMainMenu();
            this.ventana.dispose();
        }
        catch (IOException | OWASPException ex)
        {
            JOptionPane.showMessageDialog(ventana, "Error: " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void salir(boolean closing)
    {
        if(!closing)
            this.ventana.dispose();
        new CMainMenu();
    }
}
