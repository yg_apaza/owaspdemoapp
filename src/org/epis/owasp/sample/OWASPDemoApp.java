package org.epis.owasp.sample;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.epis.owasp.sample.controller.CAuthentication;
import org.epis.owasp.sample.controller.ServerSocket;
import org.epis.owasp.SecuredMessage;

public class OWASPDemoApp
{
    public static KeyStore keyStore;
    public static PublicKey myPublicKey;
    public static PrivateKey myPrivateKey;
    public static ArrayList<SecuredMessage> messages = new ArrayList<SecuredMessage>();
    public static Map<String, PublicKey> contacts = new HashMap<String, PublicKey>();
    public static ServerSocket server;
    
    public static void main(String[] args)
    {
        try
        {
            new CAuthentication ();
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream("demo.properties");
            prop.load(inputStream);
            String port = prop.getProperty("port");
            server = new ServerSocket(Integer.parseInt(port));
            server.startServer();
        }
        catch (IOException ex)
        {
            Logger.getLogger(OWASPDemoApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
